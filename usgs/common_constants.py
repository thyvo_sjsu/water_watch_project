import os

HOME_DIR = os.path.expanduser('~')
DATA_RAW_DIR = os.path.join(HOME_DIR, 'data/usgs_raw')
DATA_PROCESSED_DIR = os.path.join(HOME_DIR, 'data/usgs_processed')

MAPPING_SENSOR_ID_LIST = [
    {
        "site_code": 11128500,
        "parameter_code": 45807197,
        "sensor_id": 1
    },
    {
        "site_code": 11128500,
        "parameter_code": 45807202,
        "sensor_id": 2
    },
    {
        "site_code": 11128500,
        "parameter_code": 45807295,
        "sensor_id": 3
    },
    {
        "site_code": 11128500,
        "parameter_code": 45809894,
        "sensor_id": 4
    },
    {
        "site_code": 11128500,
        "parameter_code": 45807042,
        "sensor_id": 5
    },
    {
        "site_code": 11133000,
        "parameter_code": 45807202,
        "sensor_id": 6
    },
    {
        "site_code": 11133000,
        "parameter_code": 45807295,
        "sensor_id": 7
    },
    {
        "site_code": 11133000,
        "parameter_code": 52280502,
        "sensor_id": 8
    },
    {
        "site_code": 11133000,
        "parameter_code": 45807042,
        "sensor_id": 9
    },
    {
        "site_code": 11133000,
        "parameter_code": 45807197,
        "sensor_id": 10
    },
    {
        "site_code": 11125605,
        "parameter_code": 45807202,
        "sensor_id": 11
    },
    {
        "site_code": 11125605,
        "parameter_code": 45807295,
        "sensor_id": 12
    },
    {
        "site_code": 11125605,
        "parameter_code": 45807042,
        "sensor_id": 13
    },
    {
        "site_code": 11125605,
        "parameter_code": 45807197,
        "sensor_id": 14
    },
    {
        "site_code": 11074000,
        "parameter_code": 45807295,
        "sensor_id": 15
    },
    {
        "site_code": 11074000,
        "parameter_code": 45807042,
        "sensor_id": 16
    },
    {
        "site_code": 11074000,
        "parameter_code": 45807197,
        "sensor_id": 17
    },
    {
        "site_code": 11074000,
        "parameter_code": 45807202,
        "sensor_id": 18
    },
    {
        "site_code": 11044000,
        "parameter_code": 45809894,
        "sensor_id": 19
    },
    {
        "site_code": 11044000,
        "parameter_code": 45810855,
        "sensor_id": 20
    },
    {
        "site_code": 11044000,
        "parameter_code": 45807042,
        "sensor_id": 21
    },
    {
        "site_code": 11044000,
        "parameter_code": 45807197,
        "sensor_id": 22
    },
    {
        "site_code": 11044000,
        "parameter_code": 45807202,
        "sensor_id": 23
    },
    {
        "site_code": 11044000,
        "parameter_code": 45807295,
        "sensor_id": 24
    }
]
