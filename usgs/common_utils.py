from common_constants import *
import os
import errno
import dateutil.parser
import requests
import json
import time

SLEEP_TIME_BETWEEN_DB_POSTS_SEC = 5
ACTIVE_RATIO_THRESHOLD = 0.5
NA_VALUE = -999999.0
(ACTIVE_STATUS, INACTIVE_STATUS) = ('ACTIVE', 'INACTIVE')


# ===============================================================================
# Sensor data processing functions
# ===============================================================================

def retrieve_db_sensor_id_from_mapping_list(station_id, parameter_id):
    result = 0
    for sensor_db_info in MAPPING_SENSOR_ID_LIST:
        if (str(sensor_db_info['site_code']) == str(station_id)) and \
                (str(sensor_db_info['parameter_code']) == str(parameter_id)):
            return sensor_db_info['sensor_id']
    return result


def batch_insert_sensor_data(sensor_data_list, url='http://localhost:8082',
                             token='7a08c34ad15437d56e5bbd379f94377be8e44bc0'):
    """[{'sensor': 1, 'sensor_data_value': '9.00', 'sensor_data_dateTime': '2013-01-02T00:00:00Z'},
    {'sensor': 2, 'sensor_data_value': '15.800', 'sensor_data_dateTime': '2013-01-02T00:00:00Z'}]"""
    url += '/sensor_data/'

    print('sending sensor data POST request to ' + url)
    headers = {'Authorization': 'Token {}'.format(token)}
    result = requests.post(url, headers=headers, json=sensor_data_list, verify=False)
    print(result.status_code)


def insert_sensor_maintenance_history(sensor_maintenance_history_data, url='http://localhost:8082',
                                      token='7a08c34ad15437d56e5bbd379f94377be8e44bc0'):
    """{'dateTime': '2018-04-05T00:00:00Z', 'status': 'ACTIVE', 'sensor': 25}"""
    url += '/sensor_maintenance_history/'

    print('sending maintenance history POST request to ' + url)
    headers = {'Authorization': 'Token {}'.format(token)}
    result = requests.post(url, headers=headers, json=sensor_maintenance_history_data, verify=False)
    print(result.status_code)


def parse_sensor_info_json_file(json_path):
    sensor_info = {}
    with open(json_path, 'r') as f:
        rd = json.load(f)
        raw_sensor_data = rd['value']['timeSeries']
        for s in raw_sensor_data:
            # get sensor code
            sensor_code = s['variable']['variableCode'][0]['variableID']
            # build sensor info dict
            si = {
                'unit': s['variable']['unit']['unitCode'],
                'name': s['variable']['variableName'],
                'description': s['variable']['variableDescription'],
                'na_value': s['variable']['noDataValue']
            }
            # add si into sensor_info
            sensor_info[sensor_code] = si
    return sensor_info


def parse_sensor_data_json_file_for_csv(json_path, station_id):
    sensor_data = {}
    with open(json_path, 'r') as f:
        rd = json.load(f)
        raw_sensor_data = rd['value']['timeSeries']
        for s in raw_sensor_data:
            # get sensor code
            parameter_code = s['variable']['variableCode'][0]['variableID']
            # build sensor data as list of {'timestamp': timestamp, 'value': value}

            sensor_db_id = retrieve_db_sensor_id_from_mapping_list(station_id, parameter_code)
            if sensor_db_id != 0:
                data_list = []
                for v in s['values'][0]['value']:
                    data_list.append({'timestamp': v['dateTime'], 'value': v['value']})
                # add sd into sensor_data
                sensor_data[parameter_code] = data_list

    return sensor_data


def parse_sensor_data_json_file_for_db(json_path, station_id, host, token, date_4_status=None):
    with open(json_path, 'r') as f:
        rd = json.load(f)
        raw_sensor_data = rd['value']['timeSeries']
        for s in raw_sensor_data:
            # get sensor code
            parameter_code = s['variable']['variableCode'][0]['variableID']
            # build sensor data as list of {'timestamp': timestamp, 'value': value}

            sensor_db_id = retrieve_db_sensor_id_from_mapping_list(station_id, parameter_code)
            print 'sensor_db_id:', sensor_db_id
            if sensor_db_id != 0:
                data_list = []
                for v in s['values'][0]['value']:
                    data_list.append({'sensor': sensor_db_id,
                                      'sensor_data_dateTime': v['dateTime'],
                                      'sensor_data_value': v['value']})
                # insert data in the file
                print 'Do batch insert for data in file', json_path, 'with parameter code', parameter_code
                batch_insert_sensor_data(data_list, host, token)

                if date_4_status is not None:
                    # ASSUMPTION-- if date_4_status is not None, then:
                    #   - input data is daily, i.e., only within a day
                    #   - sensor status is calculated and inserted for that day
                    sensor_status, date_1st_timestamp = calculate_sensor_status(data_list)
                    if date_1st_timestamp is None:
                        date_1st_timestamp = date_4_status.isoformat()
                    sensor_status_data = {'dateTime': date_1st_timestamp,
                                          'status': sensor_status,
                                          'sensor': sensor_db_id}
                    insert_sensor_maintenance_history(sensor_status_data, host, token)

                print('Sleep for {} seconds between POST requests'.format(SLEEP_TIME_BETWEEN_DB_POSTS_SEC))
                time.sleep(SLEEP_TIME_BETWEEN_DB_POSTS_SEC)


def calculate_sensor_status(data_list_per_sensor):
    date_1st_timestamp = None
    date_status = INACTIVE_STATUS

    data_len = len(data_list_per_sensor)
    if data_len == 0:
        return date_status, date_1st_timestamp

    valid_value_count = 0
    for record in data_list_per_sensor:
        if record['sensor_data_value'] > NA_VALUE + 1:
            valid_value_count += 1
        if (date_1st_timestamp is None) or (record['sensor_data_dateTime'] < date_1st_timestamp):
            date_1st_timestamp = record['sensor_data_dateTime']

    if valid_value_count > int(ACTIVE_RATIO_THRESHOLD * data_len):
        date_status = ACTIVE_STATUS

        return date_status, date_1st_timestamp


# ===============================================================================
# Misc. utility functions
# ===============================================================================

def get_dt_from_any_string(s):
    d = dateutil.parser.parse(s)
    return d


def safe_make_dir(directory):
    print 'Trying to create', directory
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def get_all_files_endswith(dir_path, template='.json'):
    return [os.path.join(dir_path, f) for f in os.listdir(dir_path) if f.endswith(template)]
