from common_constants import *
import common_utils as utils
import json
import csv
import sys
import os


# ===============================================================================
# Supporting functions
# ===============================================================================

def export_sensor_info(output_dir_path, sensor_info, duration_str):
    sensor_info_json_path = os.path.join(output_dir_path, 'info_' + duration_str + '.json')
    with open(sensor_info_json_path, 'w') as f:
        json.dump(sensor_info, f, indent=4, sort_keys=True)


def export_sensor_data(output_dir_path, sensor_data, duration_str):
    # each sensor goes to a csv file
    for parameter_id, sensor_data_list in sensor_data.items():
        sensor_data_csv_path = os.path.join(output_dir_path, str(parameter_id) + '_' + duration_str + '.csv')
        with open(sensor_data_csv_path, 'w') as f:  # append
            csv_writer = csv.DictWriter(f, fieldnames=['timestamp', 'value'])
            for time_value_dict in sensor_data_list:
                csv_writer.writerow(time_value_dict)


def process_station_data_for_csv(station_id, input_json_list, output_processed_data_dir):
    output_info_path = os.path.join(output_processed_data_dir, station_id + '_info')
    output_data_path = os.path.join(output_processed_data_dir, station_id + '_data')
    if len(input_json_list) > 0:
        utils.safe_make_dir(output_info_path)
        utils.safe_make_dir(output_data_path)
    for json_file in input_json_list:
        print 'Processing', json_file
        basename = os.path.basename(json_file).split('.')[0]
        start_date_str = basename.split('_')[1]
        end_date_str = basename.split('_')[2]
        sensor_info = utils.parse_sensor_info_json_file(json_file)
        export_sensor_info(output_info_path, sensor_info, start_date_str + '_' + end_date_str)
        sensor_data = utils.parse_sensor_data_json_file_for_csv(json_file, station_id)
        export_sensor_data(output_data_path, sensor_data, start_date_str + '_' + end_date_str)


def process_station_data_for_db(station_id, input_json_list, host, token):
    for json_file in input_json_list:
        print 'Processing', json_file
        utils.parse_sensor_data_json_file_for_db(json_file, station_id, host, token)


# ===============================================================================
# Main program
# ===============================================================================

USAGE_TEXT = '''
USAGE1: python %s [option: db] [input_raw_data_dir] [output_processed_data_dir: only works for csv option]
USAGE2: python %s [option: db] [input_raw_data_dir] [host] [token]
'''


def usage(script_name):
    print USAGE_TEXT % script_name


def main(argv):
    script_name = argv[0]
    # usage(script_name)
    option = 'csv'
    if len(argv) > 1:
        option = argv[1]
    print option

    raw_data_dir = DATA_RAW_DIR
    if len(argv) > 2:
        raw_data_dir = argv[2]
    else:
        print 'Using', raw_data_dir, 'as input raw data dir...'

    # browse the list of immediate sub-dirs
    for sub_name in os.listdir(raw_data_dir):
        subdir = os.path.join(raw_data_dir, sub_name)
        if os.path.isdir(subdir):
            # name is the station id
            station_id = sub_name
            input_json_list = utils.get_all_files_endswith(subdir, template='.json')
            if option == 'csv':
                processed_data_dir = DATA_PROCESSED_DIR
                if len(argv) > 3:
                    processed_data_dir = argv[3]
                else:
                    print 'Using', processed_data_dir, 'as output processed data dir...'

                process_station_data_for_csv(station_id, input_json_list, processed_data_dir)

            else:
                host = 'http://localhost:8082/sensor_data/'
                token = '7a08c34ad15437d56e5bbd379f94377be8e44bc0'
                if len(argv) > 3:
                    host = argv[3]
                if len(argv) > 4:
                    token = argv[4]
                print 'Insert data from raw data to real db'
                process_station_data_for_db(station_id, input_json_list, host, token)


if __name__ == '__main__':
    main(sys.argv)
