import requests, json
import sys, os

DATA_RAW_DIR = './data_raw'

#===============================================================================
# Supporting functions
#===============================================================================

def get_all_stations_in_state(state_code, station_file):
    url_to_usgs = 'https://waterservices.usgs.gov/nwis/iv/?format=json&stateCd={}&siteStatus=all'.format(state_code)
    print 'Querying ' + url_to_usgs
    resp = requests.get(url_to_usgs)
    if resp.status_code != 200:
        print resp.text
        return resp.status_code
    data = json.loads(resp.text)
    with open(station_file, 'w') as outfile:
        json.dump(data, outfile)


#===============================================================================
# Main program
#===============================================================================

USAGE_TEXT = '''
USAGE: python %s [state_code]
Example state code: ca for California
'''

def usage(scriptname):
    print USAGE_TEXT % scriptname


def slowly_query_data_from_all_stations(station_list):
    pass


def main(argv):

    scriptname = argv[0]
    usage(scriptname)

    state_code = 'ca'
    if len(argv) > 1:
        state_code = argv[1]

    station_file_name = 'stations_' + state_code + '.json'
    station_file = os.path.join(DATA_RAW_DIR, station_file_name)

    if not os.path.isfile(station_file):
        # station data is not available yet, get it from usgs
        print 'Station data does not exist, will query it from USGS'
        get_all_stations_in_state(state_code, station_file)
    print 'Station data file: ' + station_file

    # parse the station data to get list of stations
    station_list = []
    with open(station_file, 'r') as f:
        raw_station_data = json.load(f)
        station_info = {}
        for raw_info in raw_station_data['value']['timeSeries']:
            station_info['fullCode'] = raw_info['name']
            station_info['siteName'] = raw_info['sourceInfo']['siteName']
            station_info['siteCode:agency'] = raw_info['sourceInfo']['siteCode'][0]['agencyCode']
            station_info['siteCode:network'] = raw_info['sourceInfo']['siteCode'][0]['network']
            station_info['siteCode:value'] = raw_info['sourceInfo']['siteCode'][0]['value']
            station_info['hasMoreThanOneSiteCode'] = True if len(raw_info['sourceInfo']['siteCode']) > 1 else False
            station_list.append(station_info)
    print 'Number of stations: ' + len(station_list)
    
    # slowly query data from all stations
    slowly_query_data_from_all_stations(station_list)

if __name__ == '__main__':
    main(sys.argv)
