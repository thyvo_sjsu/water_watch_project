from common_constants import *
import common_utils as utils
import requests
import json
import time
import sys
import os
from datetime import datetime, timedelta

DEFAULT_STATION_LIST = ['11044000', '11074000', '11125605', '11133000', '11128500']

PAUSING_TIME_BETWEEN_BATCHES_IN_SEC = 10


# ===============================================================================
# Supporting functions
# ===============================================================================

def generate_query_and_out_file_path(station, start_dt, end_dt):
    start_dt_str = start_dt.strftime('%Y-%m-%d')
    end_dt_str = end_dt.strftime('%Y-%m-%d')
    url = 'https://waterservices.usgs.gov/nwis/iv/?format=json&sites={0}&startDT={1}&endDT={2}'.format(
        station, start_dt_str, end_dt_str)
    out_dir_path = os.path.join(DATA_RAW_DIR, station)
    out_file_path = os.path.join(out_dir_path, station + '_' + start_dt_str + '_' + end_dt_str + '.json')
    return url, out_dir_path, out_file_path


def download_and_save_data(query_url, out_dir_path, out_file_path):
    print 'Querying ' + query_url
    resp = requests.get(query_url, verify=False)
    if resp.status_code != 200:
        print resp.text
        return resp.status_code
    data = json.loads(resp.text)
    utils.safe_make_dir(out_dir_path)
    with open(out_file_path, 'w') as outfile:
        print 'Saving data to', out_file_path
        json.dump(data, outfile)


# ===============================================================================
# Main program
# ===============================================================================

USAGE_TEXT = '''
USAGE: python %s <start_date_in_YYYY-MM-DD> <end_date_in_YYYY-MM-DD> [list_of_stations_in_csv_w/o_space]

Example: to download data from 2016-01-01 to 2017-12-31
    python %s 2016-01-01 2017-12-31 11044000,11074000,11125605,11133000,11128500
'''


def usage(script_name):
    print USAGE_TEXT % (script_name, script_name)
    sys.exit(1)


def main(argv):
    script_name = argv[0]
    if len(argv) < 3:
        usage(script_name)
    start_dt_str = argv[1]
    end_dt_str = argv[2]

    station_list = DEFAULT_STATION_LIST
    if len(argv) > 3:
        station_list = argv[3].split(',')

    start_dt = datetime.strptime(start_dt_str, '%Y-%m-%d')
    end_dt = datetime.strptime(end_dt_str, '%Y-%m-%d')

    start_dt_to_query = start_dt
    while start_dt_to_query <= end_dt:
        end_dt_to_query = start_dt_to_query + timedelta(days=6)
        if end_dt_to_query > end_dt:
            end_dt_to_query = end_dt
        print 'Query start date:', start_dt_to_query.strftime('%Y-%m-%d')
        print 'Query end date:', end_dt_to_query.strftime('%Y-%m-%d')

        for station in station_list:
            print 'Downloading data for station:', station
            url, out_dir_path, out_file_path = generate_query_and_out_file_path(station,
                                                                                start_dt_to_query, end_dt_to_query)
            download_and_save_data(url, out_dir_path, out_file_path)

        # sleep for some time before moving to next week
        print 'Pausing for {} seconds before moving on...'.format(PAUSING_TIME_BETWEEN_BATCHES_IN_SEC)
        time.sleep(PAUSING_TIME_BETWEEN_BATCHES_IN_SEC)
        start_dt_to_query += timedelta(days=7)


if __name__ == '__main__':
    main(sys.argv)
