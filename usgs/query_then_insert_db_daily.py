from common_constants import *
import common_utils as utils
import requests
import json
import sys
import os
import pytz
from datetime import datetime, timedelta

DEFAULT_STATION_LIST = ['11044000', '11074000', '11125605', '11133000', '11128500']

PAUSING_TIME_BETWEEN_BATCHES_IN_SEC = 10


# ===============================================================================
# Supporting functions
# ===============================================================================

def generate_query_and_out_file_path(station, start_dt, end_dt):
    start_dt_str = start_dt.strftime('%Y-%m-%d')
    end_dt_str = end_dt.strftime('%Y-%m-%d')
    url = 'https://waterservices.usgs.gov/nwis/iv/?format=json&sites={0}&startDT={1}&endDT={2}'.format(
        station, start_dt_str, end_dt_str)
    out_dir_path = os.path.join(DATA_RAW_DIR, station)
    out_file_path = os.path.join(out_dir_path, station + '_' + start_dt_str + '_' + end_dt_str + '.json')
    return url, out_dir_path, out_file_path


def download_and_save_data(query_url, out_dir_path, out_file_path):
    print 'Querying ' + query_url
    resp = requests.get(query_url)
    if resp.status_code != 200:
        print resp.text
        return resp.status_code
    data = json.loads(resp.text)
    utils.safe_make_dir(out_dir_path)
    with open(out_file_path, 'w') as outfile:
        print 'Saving data to', out_file_path
        json.dump(data, outfile)


def process_station_data_for_db(station_id, input_json_list):
    for json_file in input_json_list:
        print 'Processing', json_file
        utils.parse_sensor_data_json_file_for_db(json_file, station_id)


# ===============================================================================
# Main program
# ===============================================================================

USAGE_TEXT = '''
USAGE: python %s [host] [token] [list_of_stations_in_csv_w/o_space]
Example of Host: http://localhost:8082 OR http://water-watch-project-dev.us-west-2.elasticbeanstalk.com

Query data then insert to db for yesterday. Default list of stations: 11044000,11074000,11125605,11133000,11128500
'''


def usage(script_name):
    print USAGE_TEXT % script_name


def main(argv):
    script_name = argv[0]
    host = 'http://localhost:8082'
    token = '7a08c34ad15437d56e5bbd379f94377be8e44bc0'
    if len(argv) > 1:
        host = argv[1]
    if len(argv) > 2:
        token = argv[2]

    station_list = DEFAULT_STATION_LIST
    if len(argv) > 3:
        station_list = argv[3].split(',')
    else:
        usage(script_name)

    ca_tz = pytz.timezone('America/Tijuana')  # timezone of Pacific Time US
    dt_to_query = (datetime.now(ca_tz) - timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
    print 'Query data for date:', dt_to_query.isoformat()

    for station in station_list:
        print 'Downloading data for station:', station
        url, out_dir_path, out_file_path = generate_query_and_out_file_path(station, dt_to_query, dt_to_query)
        download_and_save_data(url, out_dir_path, out_file_path)
        print 'Insert downloaded data to db for station:', station
        utils.parse_sensor_data_json_file_for_db(out_file_path, station, host, token, date_4_status=dt_to_query)


if __name__ == '__main__':
    main(sys.argv)
