import requests, json, sys


# some sample queries:
# https://waterservices.usgs.gov/nwis/iv/?format=json&indent=on&stateCd=ca&parameterCd=00060,00065&siteStatus=all -- be aware of the amount of data
# https://waterservices.usgs.gov/nwis/iv/?format=json&sites=USGS:11128500&startDT=2018-02-28&endDT=2018-03-01
# https://waterservices.usgs.gov/nwis/iv/?format=json&sites=USGS:11125600&startDT=2017-02-25&endDT=2017-02-26&parameterCd=00065,00010
# https://waterservices.usgs.gov/nwis/iv/?format=json&stateCd=ca&siteStatus=all
# https://waterservices.usgs.gov/nwis/iv/?format=json&sites=02231291&siteStatus=all

sample_res = requests.get('https://waterservices.usgs.gov/nwis/iv/?format=json&stateCd=ca&siteStatus=all')

if sample_res.status_code != 200:
    print sample_res.text
    sys.exit(1)

sample_data = json.loads(sample_res.text)

# print json.dumps(sample_data, indent=4, sort_keys=True)

with open('sample_data.json', 'w') as outfile:
    json.dump(sample_data, outfile, indent=4, sort_keys=True)